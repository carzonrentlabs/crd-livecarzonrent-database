﻿--select  * from  dbo.Fn_GetBookedCarCatAndAssignedCArCatName (9775276,2935)
Create Function dbo.Fn_GetBookedCarCatAndAssignedCArCatName(@BookingID int,@ClientCoID int)
returns table return
(
	select @BookingID as bookingID, isNull(h.carcatName,f.carcatName) as BookedCarCatName,
	case i.VendorCarYN when 1  then
	(
	
		select top 1 isNull(CatM.CarCatName,Cat.carCatName)+'('+MM.CarModelName+')' from CORIntVendorCarMaster as VC with (Nolock) 
		inner join CORIntCarModelMaster  as MM with(Nolock) on MM.CarModelID=VC.CarModelID 
		inner join CORIntCarCatMaster as Cat with (Nolock) on Cat.CarCatID =MM.CarCatID 
		Left outer join ConIntCompanyCarModelCategoryMaster as MCM with(Nolock) on MCM.CarModelID=VC.CarModelID  and MCM.ClientCoID = @ClientCoID and MCM.Active=1
		and cast(MCM.EffectiveDateFrom as date) <= cast(a.pickupdate as date) and MCM.excludemodel = 0
		Left join CORIntCarCatMaster as CatM with (Nolock) on CatM.CarCatID =MCM.CarCatID 		
		where VC.VendorCarID=i.CarID 
		order by isNull(MCM.EffectiveDateFrom,Getdate())  desc
		
	)
	when 0 then
	(
		select top 1 isNull(CatM.CarCatName,Cat.carCatName)+'('+MM.CarModelName+')' from CORIntCarMaster  as CM with (Nolock) 
		inner join CORIntCarModelMaster  as MM with(Nolock) on MM.CarModelID=CM.CarModelID 
		inner join CORIntCarCatMaster as Cat with (Nolock) on Cat.CarCatID =MM.CarCatID 
		Left outer join ConIntCompanyCarModelCategoryMaster as MCM with(Nolock) on MCM.CarModelID=CM.CarModelID  and MCM.ClientCoID = @ClientCoID and MCM.Active=1
		and cast(MCM.EffectiveDateFrom as date) <= cast(a.pickupdate as date) and MCM.excludemodel = 0
		Left join CORIntCarCatMaster as CatM with (Nolock) on CatM.CarCatID =MCM.CarCatID 	
		where CM.CARID=i.CarID 
		order by isNull(MCM.EffectiveDateFrom,Getdate()) desc
	)
	End as AssignedCArCatName
	
	from corintcarbooking as a with(Nolock) 
	inner join dbo.CORIntClientCoIndivMaster as c with(NOLOCK) on  RIGHT(a.BookIndivID, LEN(a.BookIndivID) - 1) = c.ClientCoIndivID     
	inner join dbo.CORIntClientCoMaster as d with(NOLOCK) on c.ClientCoID = d.ClientCoID  
	inner join dbo.corintcarmodelmaster as e with (Nolock) on e.CarModelID=a.ModelID
	inner join dbo.corintcarcatMaster as f with(nolock) on f.carcatId=e.carcatId
	left outer join ConIntCompanyCarModelCategoryMaster as g with(nolock) on g.CarModelID=a.ModelID  and g.ClientCoID=@ClientCoID and g.Active=1  and g.excludemodel = 0
	left outer join CORIntCarCatMaster as h with(Nolock) on h.CarCatID =g.Carcatid 
	left outer join CorIntDutyAllocation as i with (Nolock) on i.BookingID =a.BookingID and i.Active=1
	where a.bookingId=@BookingID and c.ClientCoID=@ClientCoID
)