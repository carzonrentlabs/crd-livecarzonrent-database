USE [LiveCarzonrent_CRD]
GO

/****** Object:  StoredProcedure [dbo].[Prc_GetSMSDataNew]    Script Date: 11/13/2018 15:22:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[Prc_GetSMSDataNew]
(@Bookingid INT) 
--exec Prc_GetSMSDataNew 9313420
--exec Prc_GetSMSDataNew 8871485
--exec Prc_GetSMSDataNew 8863334 
--exec Prc_GetSMSDataNew 8847450 
--exec Prc_GetSMSDataNew 8860267 
AS 
BEGIN 
	SELECT CB.BookingID, LTRIM(RTRIM(ISNULL(IndivM.FName,''))) as GuestName 
	, LTRIM(RTRIM(ISNULL(IndivM.LName,''))) as GuestLastName 
	, CB.PickUpDate, CM.cityname, CB.PickUpTime, ISNULL(CCM.ClientPOC,'') as ClientPOC 
	, ISNULL(IndivM.Phone1,'') as GuestMobile, ISNULL(IndivM.Phone2,'') as GuestMobile2, CCM.clientconame 
	, (CASE WHEN DS.Bookingid is null THEN  
	(CASE WHEN DA.VendorCarYN = 1 THEN 
	(SELECT VCM.regnno FROM dbo.corintvendorcarmaster as VCM WITH (NOLOCK) WHERE VCM.vendorcarid = DA.carid) 
	ELSE 
	(SELECT VCM.regnno FROM dbo.corintcarmaster as VCM WITH (NOLOCK) WHERE VCM.carid = DA.carid) 
	END) 
	ELSE 
	(CASE DS.VendorCarYN  
	WHEN 0 THEN  
	(SELECT TOP 1 regnno FROM dbo.CORIntcarmaster WHERE carid =DS.carid) 
	ELSE 
	(SELECT TOP 1 regnno FROM dbo.CORIntvendorcarmaster WHERE vendorcarid =DS.carid) 
	END)END) as CarNo 
	, (CASE WHEN DS.Bookingid IS NULL THEN  
	(CASE WHEN DA.VendorChauffYN = 1 THEN 
	(SELECT ISNULL(VCM.Fname,'') --+ ' '+ ISNULL(VCM.Lname,'')  
	FROM dbo.CORIntVendorChaufMaster as VCM WITH (NOLOCK) WHERE VCM.vendorChauffeurid = DA.ChauffeurID) 
	ELSE 
	(SELECT ISNULL(VCM.Fname,'') --+ ' '+ ISNULL(VCM.Lname,'')  
	FROM dbo.corintchauffeurmaster as VCM WITH (NOLOCK) WHERE VCM.Chauffeurid = DA.ChauffeurID) 
	END) 
	ELSE 
	(CASE DS.VendorChauffYN  
	WHEN 0 THEN  
	(SELECT TOP 1 (CASE LEN(LTRIM(RTRIM(ISNULL(FName,'')))) WHEN 1 THEN LTRIM(RTRIM(ISNULL(FName,''))) +' ' + LTRIM(RTRIM(ISNULL(LName,''))) ELSE  
	LTRIM(RTRIM(ISNULL(FName,''))) END) FROM dbo.CORIntChauffeurMaster WHERE ChauffeurID =DS.ChauffeurID) 
	ELSE 
	(SELECT TOP 1 (CASE LEN(LTRIM(RTRIM(ISNULL(FName,'')))) WHEN 1 THEN LTRIM(RTRIM(ISNULL(FName,''))) +' ' + LTRIM(RTRIM(ISNULL(LName,''))) ELSE  
	LTRIM(RTRIM(ISNULL(FName,''))) END) FROM dbo.CORIntVendorChaufMaster  WHERE VendorChauffeurID=DS.ChauffeurID) 
	END)END) as ChauffeurName 
	, (CASE WHEN DS.Bookingid is null THEN  
	(CASE WHEN DA.VendorChauffYN = 1 THEN 
	(SELECT VCM.Mobile FROM dbo.CORIntVendorChaufMaster as VCM WITH (NOLOCK) WHERE VCM.vendorChauffeurid = DA.ChauffeurID) 
	ELSE 
	(SELECT VCM.Mobile FROM dbo.corintchauffeurmaster as VCM WITH (NOLOCK) WHERE VCM.Chauffeurid = DA.ChauffeurID) 
	END) 
	ELSE 
	(CASE DS.VendorChauffYN  
	WHEN 0 THEN (SELECT TOP 1 ISNULL(Mobile,'') FROM dbo.CORIntChauffeurMaster WHERE ChauffeurID =DS.ChauffeurID) 
	ELSE 
	(SELECT TOP 1 ISNULL(Mobile,'') FROM dbo.CORIntVendorChaufMaster  WHERE VendorChauffeurID=DS.ChauffeurID) 
	END) END) as ChauffeurMobile 
	--, DS.ChauffeurID, DS.VendorChauffYN 
	--, CASE WHEN CB.facilitatorid is null THEN '' ELSE ISNULL(FM.Phone1,'') END as FacilitatorMobile 
	,  ISNULL(ISNULL(FM.Phone1,'')+','+ISNULL(email.MobileNo,''),'')  as FacilitatorMobile 
	--, CASE WHEN CB.facilitatorid is null THEN '' ELSE ISNULL(FM.Phone1,'')+''+ISNULL(email.MobileNo,'') END as FacilitatorMobile 
	, CASE WHEN CB.facilitatorid IS NULL THEN '' ELSE ISNULL(FM.Phone2,'') END as FacilitatorMobile2 
	, left(REPLACE(REPLACE(REPLACE(REPLACE(CB.pickupadd,'<br />',''),CHAR(13),''),CHAR(10),''),CHAR(9),''), 77) as PickupAdd 
	, REPLACE(REPLACE(REPLACE(REPLACE(CB.pickupadd,'<br />',''),CHAR(13),''),CHAR(10),''),CHAR(9),'') as PickupAddComplete 
	, cb.STATUS 
	, ISNULL(ds.feedbackshorturl,'') as FeedbackURL, ISNULL(InvoiceDownloadShortUrl,'NoURL')as InvoiceDownloadShortUrl 
	, Penality.PenalityAmount, CVM.carvendorname as VendorName, CB.TrackingLink 
	, DATEDIFF(mi, GETDATE(), DATEADD(mi, CAST(RIGHT(CB.PickupTime,2) as int),DATEADD(HH, CAST(LEFT(CB.PickupTime,2) AS INT), CB.Pickupdate))) as TimeDiff 
	--, (CASE WHEN ISNULL(DS.ManualClosedReason,'')<>'' THEN 'Incorrect COR Drive usage' ELSE '' END) as ManualClosedReason 
	, (CASE WHEN ISNULL(DS.ManualClosedReason,'')='' THEN 'Incorrect COR Drive usage' ELSE ds.ManualClosedReason END) as ManualClosedReason 
	, Case CCM.billingbasis WHEN 'PP' THEN DS.KMClose-DS.pointopeniningkm ELSE  DS.KMIn-DS.KMOut END as TotalKMused 
	, REPLACE(REPLACE(REPLACE(CI.TotalHrsUsed,'.25','.15'),'.50','.30'),'.75','.45') as TotalHrsUsed 
	, CI.ParkTollChages,CI.TotalCost,ISNULL(CCM.ClientNameforSMS,'Carzonrent')ClientNameforSMS 
	, CASE WHEN ISNULL(CB.contdsid, 0)<>0 AND CB.pickuptime=0500 THEN 1 ELSE 0 END IsPickUpConfirmYN 
	, AdditionalNo.MobileNo AS AdditionalMobileNo  
	, IndivM.EmergencyContacts, CCM.SecurityHelpDeskNumber, CCM.SecurityHelpDeskEmail
	, (CASE CM.cordriveyn WHEN 1 THEN UM.OperationContactNo ELSE '' END) AS OperationContactNo
	, CB.ApprovalAmt
	, (case when mv.bookingid is null then (case when preauth.BookingID is null then 0 else 1 end) else 1 end) as PreauthYN, CCM.ClientCoID
	, (case when CB.Status='C' then 
	CONVERT(varchar(6), DATEDIFF(second, CONVERT(DATETIME, CONVERT(CHAR(8), DS.GuestOpDate , 112) ) + convert(time(0),LEFT(DS.GuestOpTime,2) +':'+ RIGHT(DS.GuestOpTime,2)) , CONVERT(DATETIME, CONVERT(CHAR(8), DS.GuestClDate, 112) ) + convert(time(0),LEFT(DS.GuestClTime ,2) +':'+ RIGHT(DS.GuestClTime,2)))/3600)
	+ ':'
	+ RIGHT('0' + CONVERT(varchar(2), (DATEDIFF(second, CONVERT(DATETIME, CONVERT(CHAR(8), DS.GuestOpDate , 112) ) + convert(time(0),LEFT(DS.GuestOpTime,2) +':'+ RIGHT(DS.GuestOpTime,2)) , CONVERT(DATETIME, CONVERT(CHAR(8), DS.GuestClDate, 112) ) + convert(time(0),LEFT(DS.GuestClTime,2) +':'+ RIGHT(DS.GuestClTime,2))) % 3600) / 60), 2)
	+ ':'
	+ RIGHT('0' + CONVERT(varchar(2), DATEDIFF(second, CONVERT(DATETIME, CONVERT(CHAR(8), DS.GuestOpDate , 112) ) + convert(time(0),LEFT(DS.GuestOpTime,2) +':'+ RIGHT(DS.GuestOpTime,2)) , CONVERT(DATETIME, CONVERT(CHAR(8), DS.GuestClDate, 112) ) + convert(time(0),LEFT(DS.GuestClTime,2) +':'+ RIGHT(DS.GuestClTime,2))) % 60), 2)
	when CB.Status='O' then
	CONVERT(varchar(6), DATEDIFF(second, CONVERT(DATETIME, CONVERT(CHAR(8), cormeter.GuestOpDate , 112) ) + convert(time(0),LEFT(cormeter.GuestOpTime,2) +':'+ RIGHT(cormeter.GuestOpTime,2)) , CONVERT(DATETIME, CONVERT(CHAR(8), cormeter.GuestClDate, 112) ) + convert(time(0),LEFT(cormeter.GuestClTime ,2) +':'+ RIGHT(cormeter.GuestClTime,2)))/3600)
	+ ':'
	+ RIGHT('0' + CONVERT(varchar(2), (DATEDIFF(second, CONVERT(DATETIME, CONVERT(CHAR(8), cormeter.GuestOpDate , 112) ) + convert(time(0),LEFT(cormeter.GuestOpTime,2) +':'+ RIGHT(cormeter.GuestOpTime,2)) , CONVERT(DATETIME, CONVERT(CHAR(8), cormeter.GuestClDate, 112) ) + convert(time(0),LEFT(cormeter.GuestClTime,2) +':'+ RIGHT(cormeter.GuestClTime,2))) % 3600) / 60), 2)
	+ ':'
	+ RIGHT('0' + CONVERT(varchar(2), DATEDIFF(second, CONVERT(DATETIME, CONVERT(CHAR(8), cormeter.GuestOpDate , 112) ) + convert(time(0),LEFT(cormeter.GuestOpTime,2) +':'+ RIGHT(cormeter.GuestOpTime,2)) , CONVERT(DATETIME, CONVERT(CHAR(8), cormeter.GuestClDate, 112) ) + convert(time(0),LEFT(cormeter.GuestClTime,2) +':'+ RIGHT(cormeter.GuestClTime,2))) % 60), 2)
	end	) as TripDuration
	,(case when CB.Status='C' then DS.kmclose-DS.pointopeniningkm when CB.Status='O' then cormeter.kmclose-cormeter.pointopeniningkm end) as TripDistanceKms 
	FROM dbo.CORIntCarBooking as CB WITH (NOLOCK)  
	INNER JOIN dbo.CORIntClientCoIndivMaster as IndivM WITH (NOLOCK)  
	on IndivM.ClientCoIndivID = RIGHT(CB.BookIndivID, LEN(CB.BookIndivID)-1)  
	INNER JOIN dbo.CORIntCityMaster as CM WITH (NOLOCK) on CM.cityid = CB.pickupcityid 
	INNER JOIN dbo.CorIntUnitMaster as UM WITH (NOLOCK) ON CM.NearestUnitCityID = UM.UnitCityID
	INNER JOIN dbo.CORIntClientCoMaster as CCM WITH (NOLOCK) on CCM.clientcoid = IndivM.clientcoid 
	LEFT OUTER JOIN dbo.CORIntDS as DS WITH (NOLOCK) on DS.bookingid = CB.BookingID 
	LEFT OUTER JOIN dbo.CORIntFacilitatorMaster as FM WITH (NOLOCK) on FM.facilitatorid = CB.facilitatorid 
	LEFT OUTER JOIN dbo.CorIntInvoice as CI WITH (NOLOCK) on CI.bookingid = CB.bookingid  
	LEFT OUTER JOIN dbo.CorIntDutyAllocation as DA WITH (NOLOCK) on CB.bookingid = DA.bookingid AND DA.Active=1  
	LEFT OUTER JOIN dbo.CorIntVendorPenalityDetails as Penality WITH (NOLOCK) on Penality.BookingiD = CB.BookingID 
	LEFT OUTER JOIN dbo.corintvendorcarmaster as VCM WITH (NOLOCK) on VCM.vendorcarid = DS.carid AND ds.vendorcaryn=1 
	LEFT OUTER JOIN dbo.corintcarvendormaster as CVM WITH (NOLOCK) on CVM.carvendorid = VCM.carvendorid 
	LEFT OUTER JOIN dbo.CorIntBookingEmailSMSSenderDetails as email WITH (NOLOCK) on email.clientcoid=ccm.clientcoid  
	AND (email.cityID=CM.CityID OR (email.clientcoid=ccm.clientcoid AND email.CityId=0)) AND email.Active=1 
	LEFT OUTER JOIN dbo.CorIntDispatchSMSDetail AS AdditionalNo WITH (NOLOCK) ON AdditionalNo.BookingID = CB.BookingID 
	LEFT OUTER JOIN dbo.corintmvlog as mv WITH (NOLOCK) on mv.bookingid = CB.bookingid 
	and mv.errorcode ='000' and mv.provider ='AmexBooking'
	LEFT OUTER JOIN dbo.CorIntPreAuthStatus as preauth WITH (NOLOCK) on preauth.BookingID = CB.BookingID and preauth.PreauthStatus ='400'
	LEFT OUTER JOIN dbo.CorIntCorMeterDetails as cormeter WITH (NOLOCK) on cormeter.BookingID = CB.BookingID
	WHERE CB.BookingID = @Bookingid AND UM.UnitID != 12
END 
GO


