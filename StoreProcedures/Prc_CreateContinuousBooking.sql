Alter Proc Prc_CreateContinuousBooking
(@BookingID int, @ContPickupDate varchar(max), @ContPickuphr varchar(max), @ContPickupAdd varchar(max))
--exec Prc_CreateContinuousBooking 5553238, '2014-10-18,2014-10-19,2014-10-20,2014-10-21,2014-10-22', '0500,0500,0500,0500,0500', 'test booking1,test booking12,test booking123,test booking1234,test booking12345'
--exec Prc_CreateContinuousBooking 5673129,'/','/','/'
as
begin
	
	if @ContPickupDate = '/' and @ContPickuphr = '/' and @ContPickupAdd = '/'
	begin
		return;
	end 

	Create Table #Createcontbkg (id int identity(1,1)
	, Bookingid int
	, sno int, Pickupdate varchar(10)
	, PickupTime varchar(10), PickupAdd varchar(800), NewBookingID int)

	Create table #tempcontbkg(sno int, data varchar(800))


	insert into #Createcontbkg(sno, Pickupdate, Bookingid)
	select Id, Data, @BookingID from dbo.udf_Split(@ContPickupDate, ',')

	insert into #tempcontbkg(sno, data)
	select Id, Data from dbo.udf_Split(@ContPickuphr, ',')

	update #Createcontbkg set PickupTime = (select data from #tempcontbkg where sno = #Createcontbkg.sno)

	delete from #tempcontbkg

	--insert into #Createcontbkg(sno, PickupAdd, Bookingid)
	--select Id, Data, @BookingID from dbo.udf_Split(@ContPickupAdd, ',')
	insert into #tempcontbkg(sno, data)
	select Id, Data from dbo.udf_Split(@ContPickupAdd, ',')

	update #Createcontbkg set PickupAdd = (select data+', please confirm time and address from the guest, Pickup time to be informed by guest on completion of previous booking' 
	from #tempcontbkg where sno = #Createcontbkg.sno)

	drop table #tempcontbkg

	declare @bkidnew int

	declare @sno int
	declare @Pickupdatecont datetime
	declare @pickuptime varchar(10)
	declare @pickupadd varchar(800)

	DECLARE curson_continuous CURSOR FORWARD_ONLY
	FOR select sno, Pickupdate, PickupTime, PickupAdd from #Createcontbkg 

	OPEN curson_continuous

	FETCH NEXT FROM curson_continuous
	INTO @sno, @Pickupdatecont, @pickuptime, @pickupadd

	WHILE @@FETCH_STATUS = 0
	BEGIN

	insert into CORIntCarBooking
	(BookIndivID, FacilitatorID, Service, PaymentMode, PickUpCityID, ModelID, PickUpDate, PickUpTime, DropOffDate
	, DropOffTime, PickUpAdd, OutstationYN, AirportType, FlgtNo, ReportContact, VisitCitiesIDs, IndicatedPkgID
	, IndicatedPkgHrsTrue, IndicatedPkgHrs, IndicatedPkgKMs, IndicatedExtraHr, IndicatedExtraKM, IndicatedNightStayAmt
	, IndicatedOutStnAmt, IndicatedPrice, IndicatedDiscPC, IndicatedDepositAmt,	ServiceUnitID, ApprovalNo, CancelReason
	, Status, Remarks, CreateDate, CreatedBy, NoNight, ContDSID, HouseNo, StreetNo, Landmark, PickUpLocation, SMSStatus
	, sbooking, SubCategoryId, ApprovalAmt, trackid, transactionid, authorizationid, CCType, PaymentStatus, CCNo, EXPYYMM
	, ApprovalAmt2, ApprovalNo2, trackid2, transactionid2, authorizationid2, BookingType, serviceAtBooking
	, PreAuthNotRequire, approvedBy, approvedDate, RetailGuestName, RetailGuestMobile, Miscellaneous1, Miscellaneous2
	, Miscellaneous3, Miscellaneous4, Miscellaneous5, Miscellaneous6, Miscellaneous7, DropOffCityID, ToNFroYN, DepositAmt
	, NonWaivExcess, AddDriverName, AddDriverDL, AddDriverDLCity, AddDriverDLExpiry, SDetailID, BookerID, CarID, VendorCarYN
	, CalID, SubsidiaryID, bookername, bookerphone, XMLData, PickUp, CustomPkgYN, VoucherYN, SecurityCardYN, CCNo2, EXPYYMM2
	, BookingSource, MailFrom, MailFromDate, MailFromTime, BookingStartTime, PLat, PLon, DLat, DLon, DAddress, AuthCode
	, PGId, NoofPack, VisitedCities, RequireApprovalYN, OriginCode, refundStatus, PromotionCode, indicatedDiscAmt, DiscountCode
	, limo_id, SubLocationCost, Miscellaneous8, Miscellaneous9, Miscellaneous10,RentalType,AirPortDirection,LookupId,pickupGeoLocation,ServiceTypeid
	, DropoffGeolocation,SavedCardId,Distance,EasycabsBookingTDSCreationDate,Miscellaneous11,Miscellaneous12,Miscellaneous13,Miscellaneous14,Miscellaneous15
	, CRMComplaintYN,CRMDescription,CRMCreateDate,CorDriveDistanceMultiplicationFactor,AutoCancelIfNotAllocatedYN,AutoCancelExpiryDateTime,DutyConfirmationSentDateTime
	,ispaymatecorporatemodule,TrackingLinkLastViewedAt,TrackingLinkLastViewedStatus,TrackingLink,ServiceProviderId,OriginCityId,PickupGeoLocationUpdatedByGuestDateTime
	,IndicatedCGSTTaxPercent,IndicatedSGSTTaxPercent,IndicatedIGSTTaxPercent,IndicatedCGSTTaxAmt,IndicatedSGSTTaxAmt,IndicatedIGSTTaxAmt,IndicatedClientGSTId
	,GSTSurchargeAmount, clientcoindivid)
	select 
	BookIndivID, FacilitatorID, Service, PaymentMode, PickUpCityID, ModelID, b.PickUpDate, b.PickUpTime, b.Pickupdate
	, b.PickupTime, b.PickUpAdd, OutstationYN, AirportType, FlgtNo, ReportContact, VisitCitiesIDs, IndicatedPkgID
	, IndicatedPkgHrsTrue, IndicatedPkgHrs, IndicatedPkgKMs, IndicatedExtraHr, IndicatedExtraKM, IndicatedNightStayAmt
	, IndicatedOutStnAmt, IndicatedPrice, IndicatedDiscPC, IndicatedDepositAmt, ServiceUnitID, ApprovalNo, CancelReason
	, Status, Remarks, CreateDate, CreatedBy, NoNight, @BookingID, HouseNo, StreetNo, Landmark, PickUpLocation, SMSStatus
	, sbooking, SubCategoryId, ApprovalAmt, trackid, transactionid, authorizationid, CCType, PaymentStatus, CCNo
	, EXPYYMM, ApprovalAmt2, ApprovalNo2, trackid2, transactionid2, authorizationid2, BookingType, serviceAtBooking
	, PreAuthNotRequire, approvedBy, approvedDate, RetailGuestName, RetailGuestMobile, Miscellaneous1, Miscellaneous2
	, Miscellaneous3, Miscellaneous4, Miscellaneous5, Miscellaneous6, Miscellaneous7, DropOffCityID, ToNFroYN, DepositAmt
	, NonWaivExcess, AddDriverName, AddDriverDL, AddDriverDLCity, AddDriverDLExpiry, SDetailID, BookerID, CarID, VendorCarYN
	, CalID, SubsidiaryID, bookername, bookerphone, XMLData, PickUp, CustomPkgYN, VoucherYN, SecurityCardYN, CCNo2, EXPYYMM2
	, BookingSource, MailFrom, MailFromDate, MailFromTime, BookingStartTime, PLat, PLon, DLat, DLon, DAddress, AuthCode, PGId
	, NoofPack, VisitedCities, RequireApprovalYN, OriginCode, refundStatus, PromotionCode, indicatedDiscAmt, DiscountCode
	, limo_id, SubLocationCost, Miscellaneous8, Miscellaneous9, Miscellaneous10,RentalType,AirPortDirection,LookupID,pickupGeoLocation,ServiceTypeid
	, DropoffGeolocation,SavedCardId,Distance,EasycabsBookingTDSCreationDate,Miscellaneous11,Miscellaneous12,Miscellaneous13,Miscellaneous14,Miscellaneous15
	, CRMComplaintYN,CRMDescription,CRMCreateDate,CorDriveDistanceMultiplicationFactor,AutoCancelIfNotAllocatedYN,AutoCancelExpiryDateTime,DutyConfirmationSentDateTime
	, ispaymatecorporatemodule,TrackingLinkLastViewedAt,TrackingLinkLastViewedStatus,TrackingLink,ServiceProviderId,OriginCityId,PickupGeoLocationUpdatedByGuestDateTime
	, IndicatedCGSTTaxPercent,IndicatedSGSTTaxPercent,IndicatedIGSTTaxPercent,IndicatedCGSTTaxAmt,IndicatedSGSTTaxAmt,IndicatedIGSTTaxAmt,IndicatedClientGSTId
	,GSTSurchargeAmount, clientcoindivid
	from CORIntCarBooking as a inner join #Createcontbkg as b on a.BookingID = b.Bookingid
	where a.BookingID = @BookingID and b.sno = @sno
	
	set @bkidnew = (select @@IDENTITY)

	update #Createcontbkg set NewBookingID = @bkidnew where sno = @sno
	
	FETCH NEXT FROM curson_continuous INTO @sno, @Pickupdatecont, @pickuptime, @pickupadd

	ENd

	CLOSE curson_continuous
	DEALLOCATE curson_continuous

	declare @userid int

	set @userid = (select createdby from corintcarbooking where bookingid = @BookingID)

	insert into CorIntContinuousBooking (BookingID, PickupDate, PickupTime, PickupAdd, OriginalBookingID,Createdate, Createdby)
	select NewBookingID, Pickupdate, PickupTime, PickupAdd
	, @BookingID, getdate(), @userid from #Createcontbkg

	select Bookingid, sno, Pickupdate, PickupTime, PickupAdd, NewBookingID from #Createcontbkg  order by sno
	
	drop table #Createcontbkg

end